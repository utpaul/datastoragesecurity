package utpaul.com;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.JOptionPane;

public class AuthenticationServer {
	
	Connection conn =null;
	KeyGen encryptvalue = new KeyGen();
	String query;
	String Checkemail= "";
	
	void checkCredentials(String email, String password , byte[] clientId){
		
		System.out.println();
		System.out.println("-------------------------AS Site Meassage---------------------------");
		System.out.println();
		conn = sqliteConnection.dbConnector();
		
		query ="select Email,Password from ClientTable where Email=? and Password =?";
		try {
			PreparedStatement pst = conn.prepareStatement(query);
			pst.setString(1, email);
			pst.setString(2, password);
			ResultSet rs = pst.executeQuery();
			int i=0;
			while(rs.next()){
				i++;
			}
			
			if(i==1){
				pst.close();
				
				
				//JOptionPane.showMessageDialog(null,"Email and Password are Match");
				System.out.println("AS Pass And then send meassage");
				
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				long currentTimestamp = timestamp.getTime();
				System.out.println("Timestamp in AS: "+currentTimestamp);
				
				String e="12893511608333472939274525433487354984623197897362468904990989557943281503156791743728014328039339292724896722619227256181360660608629174999927417531443227";
				String d="7126258926802968633585701604646070598841304552061974582089333425426305373501559941317305513269466117273754798789386615308462922132217211501654707407808278988587763862039952348810284156295742368764857101282916922058384734259976439665938316232732622949557213325522256345770592010649191521055741132646464388658185991282932931675391388517335791814748762098415278402445329096923282175225156153811480403426886171955072259086479763545833574079240750554688463616569696660189586817441674231027421683164571918906348506616022656079044017113832685347357545019070877501752769508825520251010321349593371348685483015746097235525623";
				String n="11040651774046700152475657445033782084332033568897200719183804471883154803066191755498327313779533204881039072765649667679702528158404625921597833890374082369448819930130023048241954711380173319431194524513686386914781536117469493241534930823548311656675821284879934552027751469381314182644962664835233768801593813566758389735636334300597548540436301897389813709751962365845546928625893082252464879328324563507241259873152433665877720675862058743198892258655077607563542575792346556826626913004170666141512158887217019621156870296685830781785420872304363865313257437716284928460035340268566334323673088545157841522913";
				
				//System.out.println("TGS public key:"+e+n );
				
				//System.out.println("TGS private key:"+d+n);
				
				query= "select email from ASserectKey where email ='"+email.toString()+"'";
				pst = conn.prepareStatement(query);
				rs = pst.executeQuery();
				while(rs.next()){
					Checkemail =rs.getString("email");
				}
				//System.out.println("Key email checking is:  "+Checkemail);

				rs.close();
				pst.close();
				if (email.equals(Checkemail)){
					System.out.println("Not enter");
				}
				else{
					//Storing AS keys 
					query="insert into ASserectKey (email,eValue,dValue,nValue) values (?,?,?,?)";
					
					PreparedStatement  pst1 = conn.prepareStatement(query);
					pst1.setString(1, email);
					pst1.setString(2, e);
					pst1.setString(3, d);
					pst1.setString(4, n);
				
					int Count=pst1.executeUpdate();
					boolean flag = false;
					if(Count>0)
					    flag=true;
					if(flag){
						pst1.close();
						//JOptionPane.showMessageDialog(null, "AS Inserted");
					}
					else
						JOptionPane.showMessageDialog(null, "AS key not Inserted");
					pst1.close();
				}
			
				BigInteger E = new BigInteger(String.valueOf(e));
				BigInteger D = new BigInteger(String.valueOf(d));
				BigInteger N = new BigInteger(String.valueOf(n));
				RSA rsa= new RSA(E,D,N);
				
	
				//System.out.println("Shared key in E in AS: "+ e);
				//System.out.println("Shared key in N is AS: "+ n);
				
				String keyvalue = null ;
				
				query ="select Key from ClientTable where Email='"+email.toString()+"'";
				pst = conn.prepareStatement(query);
				rs = pst.executeQuery();
				while(rs.next()){
					keyvalue =rs.getString("Key");
					System.out.println("SerectKey in String Value: "+keyvalue);
				}
				
				pst.close();
			
				byte[] decodedKey = Base64.getDecoder().decode(keyvalue);
				SecretKeySpec originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES"); 
				
				System.out.println("Client and AS Serect Key:"+originalKey);
				
				
				byte[] encryptedE= encryptvalue.Encrypteddata(originalKey, e);
				//System.out.println("Encrypted E in bytes: "+encryptedE);
				
				byte[] encryptedN= encryptvalue.Encrypteddata(originalKey, n);
				//System.out.println("Encrypted N in bytes: "+encryptedN);
				
				System.out.println("Encrypted ClientID in bytes in AS: "+ clientId);
				
				byte[] encryptedClientID = encryptvalue.Decrypteddata(originalKey, clientId);
				
				String ClientId = new String(encryptedClientID);
				//System.out.println();
				String Tas = ClientId + "-" + currentTimestamp;
				System.out.println("Hardware Address Adding Timestamp in AS: "+Tas);
				
				byte[] encryptedHardware = rsa.encrypt(Tas.getBytes());
				//String Encrypted= bytesToString(encrypted);
				System.out.println("Encrypted Hardware address in Bytes: " + encryptedHardware);
				//byte[] decryptedAS = rsa.decrypt(encrypted);
				//System.out.println("Decrypted hardware address:" + new String(decryptedAS));			
				
				Client client = new Client();
				client.messageFromAS(encryptedE, encryptedN, encryptedHardware);

			}
			else{
				JOptionPane.showMessageDialog(null, "Re Enter Email and Password");
				Firstpage.main(null);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NoSuchAlgorithmException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NoSuchPaddingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IllegalBlockSizeException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (BadPaddingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	} 

}
