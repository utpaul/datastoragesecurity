package utpaul.com;

public class OS_check {
	
	private String OS = System.getProperty("os.name").toLowerCase();
		
		int flag=0;
		
	    public int detectOS() {
	       
	    	if (isWindows()) {
	        	flag=1;
	        } else if (isMac()) {
	        	flag=2;
	        } else if (isUnix()) {
	        	flag=3;
	        } else {
	        	
	        }
			return flag;
	    }
	
	    private boolean isWindows() {
	        return (OS.indexOf("win") >= 0);
	    }
	
	    private boolean isMac() {
	        return (OS.indexOf("mac") >= 0);
	    }
	    private boolean isUnix() {
	        return (OS.indexOf( "nix") >=0 || OS.indexOf( "nux") >=0);
	    }

}
