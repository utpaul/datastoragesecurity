package utpaul.com;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
public class KeyGen {
	
	SecretKeySpec generatekey(String email, String password) throws UnsupportedEncodingException, NoSuchAlgorithmException{
		
	    String SALT2 = "deliciously salty";
	    byte[] key = (SALT2 + email + password).getBytes("UTF-8");
	    MessageDigest sha = MessageDigest.getInstance("SHA-1");
	    key = sha.digest(key);
	    key = Arrays.copyOf(key, 16);
	    SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
		return secretKeySpec;
	}
	
	 byte[] Encrypteddata(SecretKeySpec key, String data) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException{
		
		Cipher cipher = Cipher.getInstance("AES");
	    cipher.init(Cipher.ENCRYPT_MODE, key);
	    byte[] encrypted = cipher.doFinal((data).getBytes());
		return encrypted;
	}
	
	 byte[] Decrypteddata (SecretKeySpec key, byte[] encryptedValue) throws NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException, UnsupportedEncodingException{
		  
		Cipher cip = Cipher.getInstance("AES");
		cip.init(Cipher.DECRYPT_MODE, key);
		byte[] original = cip.doFinal(encryptedValue);
		return original;
	}
	
	

}
