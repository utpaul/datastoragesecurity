package utpaul.com;

import java.awt.EventQueue;

import javax.crypto.spec.SecretKeySpec;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.SystemColor;
import java.awt.Color;

import javax.swing.SwingConstants;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Base64;

import javax.swing.JComboBox;

public class RegistrationForm extends javax.swing.JFrame  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textFieldFirstname;
	private JTextField textFieldLastName;
	private JTextField textFieldEmail;
	private JTextField textFieldUserName;
	private JTextField textFieldPassword;
	private JTextField textFieldPhone;
	private JLabel labelback;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RegistrationForm frame = new RegistrationForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	Connection conn =null;
	public RegistrationForm() {
		String[] gender = new String[] {"--select--","Male", "Female"};
		String[] country = new String[] {"--select--", "Bangladesh", "Aus", "U.S.A","England","India"};
		conn = sqliteConnection.dbConnector();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 508, 501);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblRegistrationForm = new JLabel("REGISTRATION FORM");
		lblRegistrationForm.setForeground(new Color(255, 69, 0));
		lblRegistrationForm.setHorizontalAlignment(SwingConstants.CENTER);
		lblRegistrationForm.setBackground(Color.WHITE);
		lblRegistrationForm.setBounds(102, 11, 276, 41);
		lblRegistrationForm.setFont(new Font("Tahoma", Font.BOLD, 24));
		contentPane.add(lblRegistrationForm);
		
		textFieldFirstname = new JTextField();
		textFieldFirstname.setBounds(215, 110, 104, 29);
		textFieldFirstname.setUI(new HintTextFieldUI(" First Name", true));
		contentPane.add(textFieldFirstname);
		textFieldFirstname.setColumns(10);
		
		textFieldLastName = new JTextField();
		textFieldLastName.setColumns(10);
		textFieldLastName.setUI(new HintTextFieldUI(" Last Name", true));
		textFieldLastName.setBounds(329, 110, 104, 29);
		contentPane.add(textFieldLastName);
		
		textFieldEmail = new JTextField();
		textFieldEmail.setColumns(10);
		textFieldEmail.setUI(new HintTextFieldUI(" Email Here", true));
		textFieldEmail.setBounds(215, 150, 218, 29);
		contentPane.add(textFieldEmail);
		
		textFieldUserName = new JTextField();
		textFieldUserName.setColumns(10);
		textFieldUserName.setUI(new HintTextFieldUI(" Username Here", true));
		textFieldUserName.setBounds(215, 190, 218, 29);
		contentPane.add(textFieldUserName);
		
		textFieldPassword = new JTextField();
		textFieldPassword.setColumns(10);
		textFieldPassword.setUI(new HintTextFieldUI(" password Here", true));
		textFieldPassword.setBounds(215, 230, 218, 29);
		contentPane.add(textFieldPassword);
		
		textFieldPhone = new JTextField();
		textFieldPhone.setColumns(10);
		textFieldPhone.setUI(new HintTextFieldUI(" Phone No", true));
		textFieldPhone.setBounds(215, 270, 218, 29);
		contentPane.add(textFieldPhone);
		
		JComboBox<String> comboBoxGender = new JComboBox<String>(gender);
		comboBoxGender.setBounds(215, 310, 104, 23);
		contentPane.add(comboBoxGender);
		
		JComboBox<String> comboBoxCountry = new JComboBox<String>(country);
		comboBoxCountry.setBounds(329, 310, 104, 23);
		contentPane.add(comboBoxCountry);
		
		JButton btnSignUp = new JButton("SIGN UP");
		btnSignUp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String firstname =null;
				String lastname =null;
				String email =null;
				String password =null;
				String username =null;
				String phone_no =null;
				String gender_name =null;
				String country_name =null;
				
				firstname = textFieldFirstname.getText();
				lastname = textFieldLastName.getText();
				email = textFieldEmail.getText();
				password = textFieldPassword.getText();
				username = textFieldUserName.getText();
				phone_no = textFieldPhone.getText();
				gender_name = (String) comboBoxGender.getSelectedItem();
				country_name = (String) comboBoxCountry.getSelectedItem();
				
				if(passwordcheck(password))
                {
					
					System.out.println(firstname + " "+ lastname + " "+ email + " "+ password + " "+ username+ " "+ phone_no + " "+ gender_name+ " "+ country_name  );
					KeyGen keyAES = new  KeyGen();
					Md5Hassing hassing = new Md5Hassing();
					
					SecretKeySpec secretKeySpec = null;
					try {
						secretKeySpec = keyAES.generatekey(email, password);
						System.out.println(secretKeySpec);
					} catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
						e.printStackTrace();
					}
					
					try {
						email =hassing.hassingValue(email);
						password =hassing.hassingValue(password);
						System.out.println(hassing.hassingValue(username));
						System.out.println(hassing.hassingValue(password));
					} catch (NoSuchAlgorithmException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					String query ="insert into ClientTable (Firstname,Lastname,Username,Password,Email,Phone,Key,Gender,Country) values (?,?,?,?,?,?,?,?,?)";
					try {
						String encodedKey = Base64.getEncoder().encodeToString(secretKeySpec.getEncoded());
						
						PreparedStatement pst = conn.prepareStatement(query);
						pst.setString(1, firstname);
						pst.setString(2, lastname);
						pst.setString(3, username);
						pst.setString(4, password);
						pst.setString(5, email);
						pst.setString(6, phone_no);
						pst.setString(7, encodedKey);
						pst.setString(8, gender_name);
						pst.setString(9, country_name);
						
						int updateCount=pst.executeUpdate();
						
						boolean flag= false;
						if(updateCount>0)
						    flag=true;
						
						if(flag){
							pst.close();
							dispose();
							Firstpage.main(null);
						}
						else
							System.out.println("no action");
						
						
						
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}					
                }
				
				else{
					password = null;
					textFieldPassword.setText("");
					JOptionPane.showMessageDialog(null, "Enter Password(password cotain at latest 8 character)");
				}

				
			}

			private boolean passwordcheck(String s)
			{
			    int l=s.length();
			    if(l<8)
			        return false;
			    else
			    {
			        int a=0,b=0,c=0,d=0;
			        for(int i=0;i<l;i++)
			        {
			            if("0123456789".contains(s.charAt(i)+""))
			                a=1;
			            else if(("abcdefghijklmnopqrstuvwxyz").contains(s.charAt(i)+""))
			                b=1;
			             else if(("ABCDEFGHIJKLMNOPQRSTUVWXYZ").contains(s.charAt(i)+""))
			                c=1;
			             else if(("`~!@#$%^&*()-_=+[{]}\\\\|;:'\\\",<.>/?").contains(s.charAt(i)+""))
			                d=1;
			            
			        }
			     
			        a=a+b+c+d;
			        
			        if(a==4)
			            return true;
			        else
			            return false;
			    }}
			   
		});
		btnSignUp.setForeground(Color.WHITE);
		btnSignUp.setBackground(SystemColor.textHighlight);
		btnSignUp.setBounds(215, 344, 218, 23);
		contentPane.add(btnSignUp);
		
		JLabel lblNewLabel = new JLabel("");
		java.awt.Image img = new ImageIcon(this.getClass().getResource("/registration.png")).getImage();
		lblNewLabel.setIcon(new ImageIcon(img));
		lblNewLabel.setBounds(10, 132, 195, 193);
		contentPane.add(lblNewLabel);
		
		labelback = new JLabel("");
		labelback.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				dispose();
				Firstpage.main(null);
			}
		});
		labelback.setHorizontalAlignment(SwingConstants.CENTER);
		java.awt.Image imgback = new ImageIcon(this.getClass().getResource("/go-back.png")).getImage();
		labelback.setIcon(new ImageIcon(imgback));
		labelback.setBounds(22, 18, 34, 34);
		contentPane.add(labelback);
		

	}
}
