package utpaul.com;

import java.io.IOException;
import java.util.Scanner;

public class IDgenerator {

	private Scanner sc;
	String property,serial;
	
	public String getBiosSn() throws IOException{
		
		//BIOS SERIAL
	    Process process = Runtime.getRuntime().exec(new String[] { "wmic", "bios", "get", "serialnumber" });
        process.getOutputStream().close();
        sc = new Scanner(process.getInputStream());
        property=sc.next();
        serial = sc.next();
        
        return serial;	
	}
	
	public String getRamSn() throws IOException{
		
		//RAM SERIAL
	    Process process = Runtime.getRuntime().exec(new String[] { "wmic", "memorychip", "get", "serialnumber" });
        process.getOutputStream().close();
        sc = new Scanner(process.getInputStream());
        property=sc.next();
        serial = sc.next();
        return serial;	
	}
	public String getMotherboardSn() throws IOException{
		
		//MOTHERBOARD SERIAL
	    Process process = Runtime.getRuntime().exec(new String[] { "wmic", "baseboard", "get", "serialnumber" });
        process.getOutputStream().close();
        sc = new Scanner(process.getInputStream());
        property=sc.next();
        serial = sc.next();
		
        return serial;	
	}
	
	public String getHarddiskSn() throws IOException{
		
		//HARDDRIVE SERIAL
	    Process process = Runtime.getRuntime().exec(new String[] { "wmic", "diskdrive", "get", "serialnumber" });
        process.getOutputStream().close();
        sc = new Scanner(process.getInputStream());
        property=sc.next();
        serial = sc.next();
        
        return serial;	
	}
}
