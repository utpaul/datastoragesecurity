package utpaul.com;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.Color;

import javax.swing.JFileChooser;

import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.JTabbedPane;
import javax.swing.JButton;


import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

import javax.swing.JTextArea;

import java.awt.SystemColor;

import javax.swing.JTable;



public class HomePage extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	public File selectedFile= null;
	String path ="Unselected Path";
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HomePage frame = new HomePage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public HomePage() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 508, 501);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblWelcome = new JLabel("WELCOME");
		lblWelcome.setHorizontalAlignment(SwingConstants.CENTER);
		lblWelcome.setForeground(new Color(255, 69, 0));
		lblWelcome.setFont(new Font("Tahoma", Font.BOLD, 24));
		lblWelcome.setBounds(147, 11, 205, 41);
		contentPane.add(lblWelcome);
		
		JLabel lblNewLabel = new JLabel("TO CLOUD STORAGE");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(157, 48, 178, 34);
		contentPane.add(lblNewLabel);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(30, 93, 443, 355);
		contentPane.add(tabbedPane);
	
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		tabbedPane.addTab("Upload", null, panel, null);
		panel.setLayout(null);
		
		JTextArea textArea = new JTextArea();
		textArea.setBackground(SystemColor.control);
		textArea.setBounds(77, 130, 218, 22);
		panel.add(textArea);
		
		JButton btnNewButton = new JButton("Browse...");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				 JFileChooser open = new JFileChooser();
		         open.setCurrentDirectory(new File(System.getProperty("user.home")));           
		         int result = open.showOpenDialog(null);
		         if (result == JFileChooser.APPROVE_OPTION) {
		             selectedFile = open.getSelectedFile();
		             path =selectedFile.getAbsolutePath().toString();
		             System.out.println("Selected file: " + path);
		             textArea.append("File Path: "+path);
		         }
			}

		});
		btnNewButton.setBounds(304, 130, 104, 23);
		panel.add(btnNewButton);
		
		
		
		JButton button = new JButton("Upload File");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CountDownProgressBar cdpb = new CountDownProgressBar();
			}
		});
		button.setFont(new Font("Tahoma", Font.PLAIN, 13));
		button.setBounds(176, 183, 104, 23);
		panel.add(button);
		
	
		JPanel panel_download = new JPanel();
		panel_download.setBackground(Color.WHITE);
		tabbedPane.addTab("Download", null, panel_download, null);
        
		String[] columns = new String[] {
                "  Id", "File Name", "Format", "Download"
            };
             
            //actual data for the table in a 2d array
            Object[][] data = new Object[][] {
                {"    1", "jaas", ".pdf", "Click Here" },
                {"    2", "Dhoom", ".mp4", "Click Here" },
                {"    3", "Hello", ".mp3", "Click Here" },
            };
     
		table = new JTable(data, columns);
		table.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e) {
				CountDownProgressBar cdpb = new CountDownProgressBar();
			}
		});
		panel_download.add(new JScrollPane(table));


	}
}
