package utpaul.com;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.JOptionPane;

public class Client extends Firstpage{
	
	Md5Hassing hasing = new Md5Hassing();
	
	Connection conn =null;
	String query ;
	PreparedStatement pst = null;
	String keyvalue = null, Checkemail="";
	KeyGen keyAES = new  KeyGen();
	OS_check check_os= new OS_check();
	AuthenticationServer AS = new AuthenticationServer();
	IDgenerator os_hardware=new IDgenerator();
	int os_type=check_os.detectOS();
	
	String bios=null,motherboard= null,disk=null,ram=null,plantext = null, currentIp =null;
	
	byte[] encryptedValueFromAS, encryptedValueFromTGS, meassage;
	
	
	
	public void getingClientValue(String email, String password) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, SQLException {
		
		conn = sqliteConnection.dbConnector();
		SecretKeySpec secretKeySpec = null;
		
		
		System.out.println("-------------------------Client to As Site Meassage---------------------------");
		System.out.println();
		
		try {
			secretKeySpec = keyAES.generatekey(email, password);
			System.out.println("Serect key(client and Server): " + secretKeySpec);
		} catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		try {
			email = hasing.hassingValue(email);
			password = hasing.hassingValue(password);
			
		} catch (NoSuchAlgorithmException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		String encodedKey = Base64.getEncoder().encodeToString(secretKeySpec.getEncoded());
		
		query= "select email from SerectKey where email ='"+email.toString()+"'";
		
		pst = conn.prepareStatement(query);
		ResultSet rs = pst.executeQuery();
		rs = pst.executeQuery();
		while(rs.next()){
			Checkemail =rs.getString("email");
		}
		
		rs.close();
		pst.close();
		if (email.equals(Checkemail)){
			System.out.println("Not enter in Client");
		}
		else{
			query ="insert into SerectKey (email,key) values (?,?)";
			
			int updateCount = 0;
			try {
				pst = conn.prepareStatement(query);
				pst.setString(1, email);
				pst.setString(2, encodedKey);
				updateCount=pst.executeUpdate();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			
			
			boolean flag= false;
			if(updateCount>0)
			    flag=true;
			
			if(flag){
				try {
					pst.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else
			 JOptionPane.showMessageDialog(null, "Error Occure");
			pst.close();
		}
		
		if(os_type==1){
			
			try {
				bios=os_hardware.getBiosSn();
				motherboard=os_hardware.getMotherboardSn();
				disk=os_hardware.getHarddiskSn();
				ram=os_hardware.getRamSn();
				InetAddress iAddress = InetAddress.getLocalHost();
				currentIp = iAddress.getHostAddress();		
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 
			
			plantext=bios+"-"+motherboard+"-"+disk+"-"+ram;
			
		}
			
		else if (os_type==2){
				plantext=Hardware4Mac.getSerialNumber();
		}
		else if(os_type==3){
				plantext=Hardware4Nix.getSerialNumber();
		}
		plantext =plantext+"-"+currentIp;
		
		System.out.println("Hardware address:"+ plantext);
		
		byte[] encryptedClientID= keyAES.Encrypteddata(secretKeySpec, plantext);
		
		System.out.println("Encrypted Hardware address in bytes: "+ encryptedClientID);
		System.out.println("Email hassing value:"+email);
		System.out.println("Password hassing value:"+ password);
		
		AS.checkCredentials(email,password, encryptedClientID);
		
	}
	
	
	void messageFromAS (byte[] e, byte[] n, byte[] encryptedValueFromAS) throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException{
			
		

		System.out.println("-------------------------AS to Client  and Send to TGS Site Meassage---------------------------");
		System.out.println();
		
		conn = sqliteConnection.dbConnector();
		
			byte[] eParameter, nParameter;
			String email = null;
			
			eParameter =e;
			nParameter = n;
			
			byte[] decodedKey, decryptedN, decryptedE;
			BigInteger E,N;
			
			this.encryptedValueFromAS = encryptedValueFromAS;
			//System.out.println("e in client: "+ eParameter);
			//System.out.println("n in client: "+ nParameter);
			
			System.out.println("Encrypted(TGS private key) HardwareAddress in Clientsite in Bytes:"+ encryptedValueFromAS);
			
			
			
			query = "select * from SerectKey";
			try {
				PreparedStatement pst = conn.prepareStatement(query);
				ResultSet rs = pst.executeQuery();
				while(rs.next()){
					keyvalue =rs.getString("key");
					email =rs.getString("email");
					
					//System.out.println("Getting key form clientsite :  "+keyvalue);
					System.out.println("Getting SerectKey Base64encoding String: "+email);
				}
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	
			
			
			
			decodedKey = Base64.getDecoder().decode(keyvalue);
			SecretKeySpec originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
			System.out.println("Orginal Key After Decoding:"+ originalKey);
			
			decryptedN= keyAES.Decrypteddata(originalKey, nParameter);
			String nDecrypt =new String(decryptedN, "UTF8");
			//System.out.println("Decrypt String in N clientsite: "+ nDecrypt);
			
			decryptedE= keyAES.Decrypteddata(originalKey, eParameter);
			String eDecrypt =new String(decryptedE, "UTF8");
			//System.out.println("Decrypt String in E clientsite: "+ eDecrypt);
			
			E = new BigInteger(String.valueOf(eDecrypt));
			N = new BigInteger(String.valueOf(nDecrypt));
			
			//System.out.println("BigInteger E value in Clientsite: "+E);
			//System.out.println("BigInteger N value in Clientsite: "+N);
			
			if(os_type==1){
				
					try {
						bios=os_hardware.getBiosSn();
						motherboard=os_hardware.getMotherboardSn();
						disk=os_hardware.getHarddiskSn();
						ram=os_hardware.getRamSn();
						InetAddress iAddress = InetAddress.getLocalHost();
						currentIp = iAddress.getHostAddress();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			
				plantext=bios+"-"+motherboard+"-"+disk+"-"+ram;
				//System.out.println(plantext);
			}
				
			else if (os_type==2){
					plantext=Hardware4Mac.getSerialNumber();
			}
			else if(os_type==3){
					plantext=Hardware4Nix.getSerialNumber();
			}
			
			plantext = plantext +"-"+currentIp;
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			long currentTimestamp = timestamp.getTime();
			
			System.out.println("TimeStamp in Clientsite: "+currentTimestamp);
			
			plantext = plantext+"-"+currentTimestamp +"-"+ email;
			
			
			System.out.println("Clientsite Hardware address: "+ plantext);
			
			RSA rsa= new RSA(E,N);
		
			byte[] encrypted = rsa.encrypt(plantext.getBytes());
			System.out.println("Encrypt clientsite hardware address: "+encrypted);
			
			//String EncryptedTgs= bytesToString(encrypted);
			//System.out.println("Encrypted String to give TGS in Bytes: " + EncryptedTgs);
			
			base64EncodingDecoding encodebase64 = new base64EncodingDecoding();
			String emailEncode = encodebase64.Encodingbase64(email);
			//System.out.println("email encode in client:"+emailEncode);
			//String emailDecode = encodebase64.Decodingbase64(emailEncode);
			//System.out.println("Email decode in client:"+emailDecode);
			TGS tgs = new TGS();
			tgs.meassageFromUser(encrypted, encryptedValueFromAS, emailEncode);
			
		}
	
		
	void messageFromTGS (byte[] e, byte[] n, byte[] encryptedValueFromTGS) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
			
		
		System.out.println("-------------------------TGS to Client  and Send to File Server Meassage---------------------------");
		System.out.println();
		
		//System.out.println("E in client site"+e);
		//System.out.println("N in client site"+ n);
		//System.out.println("EncryptedTGS meassage"+ encryptedValueFromTGS);
		
		conn = sqliteConnection.dbConnector();
			
			byte[] eParameter, nParameter;
			String email = null;
			eParameter =e;
			nParameter = n;
			byte[] decryptedN, decryptedE;
			BigInteger D,N;
			String dValue = null, nValue = null;
			
			this.encryptedValueFromTGS = encryptedValueFromTGS;
			System.out.println("e in client: "+ eParameter);
			System.out.println("n in client: "+ nParameter);
			
			System.out.println("TGS in client in Bytes: "+encryptedValueFromTGS);
			
			query = "select dValue,nValue from ASserectKey";
			try {
				PreparedStatement pst = conn.prepareStatement(query);
				ResultSet rs = pst.executeQuery();
				
				while(rs.next()){
					dValue =rs.getString("dValue");
					nValue =rs.getString("nValue");
					System.out.println("Getting key form clientsite :  "+dValue);
					System.out.println("Getting key form clientsite :  "+nValue);
				}
				
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			D = new BigInteger(String.valueOf(dValue));
			N = new BigInteger(String.valueOf(nValue));
			
			RSA2 rsa = new RSA2(D,N);
			
			
			decryptedE = rsa.decrypt(eParameter);
			System.out.println("E in client:"+ new String(decryptedE));
			
			String DecryptedN ="14281000688569201475902954169185425618353854122659299945090336377376573702436137675440009286358810368409015898710703541660495831325057446476365443381622854999409191124034519457743488871053857537609130466341850350927931901180937996981781745005327319593250008347986273605909400832661592640592557224931050929958561850978219322999902752585890115713331148503227230931402698436807004254969121806941024790862945284164860700616292056399435892656009998468808856761877611321165236429610466060563893214285178706977845959749192386722800643691154405396872060189110460336861680877119608034933811990500215160219511782490478608644899";
			
			decryptedN = rsa.decrypt(nParameter);
			System.out.println("N in client:"+ new String(decryptedN));
			
			
			BigInteger Etgs, Ntgs;
			
			Etgs = new BigInteger(new String(decryptedE));
			Ntgs = new BigInteger(new String(DecryptedN));
			
			if(os_type==1){
				
					try {
						bios=os_hardware.getBiosSn();
						motherboard=os_hardware.getMotherboardSn();
						disk=os_hardware.getHarddiskSn();
						ram=os_hardware.getRamSn();
						InetAddress iAddress = InetAddress.getLocalHost();
						currentIp = iAddress.getHostAddress();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				
				plantext=bios+"-"+motherboard+"-"+disk+"-"+ram;
				System.out.println(plantext);
			}
				
			else if (os_type==2){
					plantext=Hardware4Mac.getSerialNumber();
			}
			else if(os_type==3){
					plantext=Hardware4Nix.getSerialNumber();
			}
			
			plantext = plantext +"-"+currentIp;
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			long currentTimestamp = timestamp.getTime();
			System.out.println("TimeStamp in Clientsite: "+currentTimestamp);
			
			plantext = plantext+"-"+currentTimestamp;
			
			System.out.println("Client Site Hardware Address: "+ plantext);
			
			RSA Rsa = new RSA(Etgs,Ntgs);
			
			byte[] encrypted = Rsa.encrypt(plantext.getBytes());
			
			System.out.println("Bytes value in Plantext in TGS: "+encrypted);
			
			FileServer fileServer = new FileServer();
			fileServer.meassageFromUser(encrypted, encryptedValueFromTGS);
			
			
			
			/*decodedKey = Base64.getDecoder().decode(keyvalue);
			SecretKeySpec originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
			System.out.println("orginalKey in clientsite: "+ originalKey);
			
			decryptedN= keyAES.Decrypteddata(originalKey, nParameter);
			String nDecrypt =new String(decryptedN, "UTF8");
			System.out.println("Decrypt String in N clientsite: "+ nDecrypt);
			
			decryptedE= keyAES.Decrypteddata(originalKey, eParameter);
			String eDecrypt =new String(decryptedE, "UTF8");
			System.out.println("Decrypt String in E clientsite: "+ eDecrypt);
			
			E = new BigInteger(String.valueOf(eDecrypt));
			N = new BigInteger(String.valueOf(nDecrypt));
			
			System.out.println("BigInteger E value in Clientsite After TGS: "+E);
			System.out.println("BigInteger N value in Clientsite After TGS: "+N);
			
			
			if(os_type==1){
				
				
				try {
					bios=os_hardware.getBiosSn();
					motherboard=os_hardware.getMotherboardSn();
					disk=os_hardware.getHarddiskSn();
					ram=os_hardware.getRamSn();
					InetAddress iAddress = InetAddress.getLocalHost();
					currentIp = iAddress.getHostAddress();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		
			 
			
			plantext=bios+"-"+motherboard+"-"+disk+"-"+ram;
			System.out.println(plantext);
		}
			
		else if (os_type==2){
				plantext=Hardware4Mac.getSerialNumber();
		}
		else if(os_type==3){
				plantext=Hardware4Nix.getSerialNumber();
		}
		
		plantext = plantext +"-"+currentIp;
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		long currentTimestamp = timestamp.getTime();
		System.out.println("TimeStamp in Clientsite: "+currentTimestamp);
		
		plantext = plantext+"-"+currentTimestamp +"-"+ email;
		
		System.out.println("Client plantext is: "+ plantext);
		
		RSA rsa= new RSA(E,N);
		
		byte[] encrypted = rsa.encrypt(plantext.getBytes());
		System.out.println("Bytes value in Plantext in TGS: "+encrypted);
		FileServer fileServer = new FileServer();
		fileServer.meassageFromUser(encrypted, encryptedValueFromTGS);*/
	}
	
	void messageFromFS (byte[] meassage) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException{
		
		
		System.out.println("-------------------------FS to Client  and Send And Succesfully---------------------------");
		System.out.println();
		
		conn = sqliteConnection.dbConnector();
		this.meassage =meassage;
		byte[] decodedKey, decryptedMeassage;
		System.out.println("client site after Fs execution: "+meassage);
		query = "select * from SerectKey";
		try {
			PreparedStatement pst = conn.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			while(rs.next()){
				keyvalue =rs.getString("key");
				System.out.println("Getting key form clientsite :  "+keyvalue);
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		decodedKey = Base64.getDecoder().decode(keyvalue);
		SecretKeySpec originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
		System.out.println("orginalKey in clientsite: "+ originalKey);
		
		decryptedMeassage= keyAES.Decrypteddata(originalKey, meassage);
		String nDecrypt =new String(decryptedMeassage, "UTF8");
		
		if (nDecrypt.equals("OK")){
			
			 JOptionPane.showMessageDialog(null, "Authenticate Successfully");
			 
			 HomePage hp = new HomePage();
			 hp.setVisible(true);
		}
		
		else{
			Firstpage.main(null);
		}
		
		
	}
	
	

}
