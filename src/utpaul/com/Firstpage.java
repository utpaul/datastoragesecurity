package utpaul.com;

import java.awt.Cursor;
import java.awt.EventQueue;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;

import java.awt.Font;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

import javax.swing.JTextField;
import javax.swing.SwingConstants;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

public class Firstpage {

	private JFrame frame;
	private JPasswordField passwordField;
	private JTextField textFieldemail;
	private JLabel lblNewLabel;
	public String Email= null;
	public String Password=null;
	private JLabel lblNewLabel_1;
	private JLabel lblPassword;

	public static void main(String[] args) {
		
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Firstpage window = new Firstpage();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}

	public Firstpage() {
		initialize();
	}

	private void initialize() {
		
		frame = new JFrame();
		frame.setBounds(100, 100, 508, 501);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(144, 316, 205, 26);
		frame.getContentPane().add(passwordField);
		
		JLabel lblNewLabel_2 = new JLabel("CLOUD STORAGE");
		lblNewLabel_2.setForeground(new Color(255, 69, 0));
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 24));
		lblNewLabel_2.setLabelFor(frame);
		lblNewLabel_2.setBounds(144, 39, 205, 41);
		frame.getContentPane().add(lblNewLabel_2);
		
		textFieldemail = new JTextField();
		textFieldemail.setBounds(144, 276, 205, 29);
		textFieldemail.setUI(new HintTextFieldUI("  Email Address...", true));
		frame.getContentPane().add(textFieldemail);
		textFieldemail.setColumns(10);
		
		
		JLabel lblNewLabel_3 = new JLabel("");
		java.awt.Image img = new ImageIcon(this.getClass().getResource("/sigin-in.png")).getImage();
		lblNewLabel_3.setIcon(new ImageIcon(img));
		lblNewLabel_3.setBounds(179, 105, 137, 132);
		frame.getContentPane().add(lblNewLabel_3);
		
		JButton btnNewButton = new JButton("SIGN IN");
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.addActionListener(new ActionListener() {
			
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				
				Email = textFieldemail.getText();
				Password = passwordField.getText();
				
				Client client = new Client();

				if(Email.length()>=8 && Password.length()>=8){
					
					try {
						try {
							client.getingClientValue(Email,Password);
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} catch (InvalidKeyException | NoSuchAlgorithmException
							| NoSuchPaddingException
							| IllegalBlockSizeException | BadPaddingException
							| UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					frame.dispose();
					
				}
				else{
					passwordField.setText("");
					textFieldemail.setText("");
					JOptionPane.showMessageDialog(null, "Re enter username and password");
				}

				
			}
		});
		btnNewButton.setBounds(144, 353, 205, 23);
		btnNewButton.setBackground(new Color(51, 153, 255));
		frame.getContentPane().add(btnNewButton);
		

		lblNewLabel = new JLabel("Create Account");
		lblNewLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				frame.dispose();
				RegistrationForm rf = new RegistrationForm();
				rf.setVisible(true);
			}
		});
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(144, 390, 200, 26);
		lblNewLabel.setCursor(new Cursor(Cursor.HAND_CURSOR));
		frame.getContentPane().add(lblNewLabel);
		
		lblNewLabel_1 = new JLabel("Email");
		lblNewLabel_1.setBackground(Color.WHITE);
		lblNewLabel_1.setForeground(Color.BLACK);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel_1.setBounds(67, 275, 56, 29);
		frame.getContentPane().add(lblNewLabel_1);
		
		lblPassword = new JLabel("Password");
		lblPassword.setForeground(Color.BLACK);
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblPassword.setBackground(Color.WHITE);
		lblPassword.setBounds(67, 314, 67, 29);
		frame.getContentPane().add(lblPassword);
		frame.setResizable(false);
	}

}
