package utpaul.com;

import javax.xml.bind.DatatypeConverter;

	public class base64EncodingDecoding {
		
		String Encodingbase64(String str){
			String encoded = DatatypeConverter.printBase64Binary(str.getBytes());
			return encoded;
		}
	
		String Decodingbase64(String str){
			String decoded = new String(DatatypeConverter.parseBase64Binary(str));;
			return decoded;
		}
}
