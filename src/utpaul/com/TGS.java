package utpaul.com;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.JOptionPane;

public class TGS {
 
		byte[] EncryptedTgs;
		byte[] encryptedValueFromAS;
		String biosAS=null,motherboardAS= null,diskAS=null,ramAS=null,currentIpAS =null,currentTimestampAS =null;
		
		String biosC=null,motherboardC= null,diskC=null,ramC=null,currentIpC =null, currentTimestampC =null;
		
		String query,keyvalue;
		Connection conn =null;
		KeyGen keyAES = new  KeyGen();
		String emailEncode =null;
	
	
	void meassageFromUser(byte[] EncryptedTgs, byte[] encryptedValueFromAS, String emailEncode) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException{
		 
		
		System.out.println("-------------------------TGS Site Meassage---------------------------");
		System.out.println();
		
		
		conn = sqliteConnection.dbConnector();
		this.encryptedValueFromAS =encryptedValueFromAS;
		this.EncryptedTgs =EncryptedTgs;
		this.emailEncode= emailEncode;
		//System.out.println("Email encoded in TGS:"+ emailEncode);
		
		base64EncodingDecoding decodeStr = new base64EncodingDecoding();
		String EmailDecode = decodeStr.Decodingbase64(emailEncode);
		//System.out.println("Email decoded in tgs: "+EmailDecode);
		
		System.out.println("User Hardware information in bytes: "+ EncryptedTgs);
		System.out.println("Ticket from Undecrypt Meassage in AS: "+ encryptedValueFromAS);
		 
		String e="12893511608333472939274525433487354984623197897362468904990989557943281503156791743728014328039339292724896722619227256181360660608629174999927417531443227";
		String d="7126258926802968633585701604646070598841304552061974582089333425426305373501559941317305513269466117273754798789386615308462922132217211501654707407808278988587763862039952348810284156295742368764857101282916922058384734259976439665938316232732622949557213325522256345770592010649191521055741132646464388658185991282932931675391388517335791814748762098415278402445329096923282175225156153811480403426886171955072259086479763545833574079240750554688463616569696660189586817441674231027421683164571918906348506616022656079044017113832685347357545019070877501752769508825520251010321349593371348685483015746097235525623";
		String n="11040651774046700152475657445033782084332033568897200719183804471883154803066191755498327313779533204881039072765649667679702528158404625921597833890374082369448819930130023048241954711380173319431194524513686386914781536117469493241534930823548311656675821284879934552027751469381314182644962664835233768801593813566758389735636334300597548540436301897389813709751962365845546928625893082252464879328324563507241259873152433665877720675862058743198892258655077607563542575792346556826626913004170666141512158887217019621156870296685830781785420872304363865313257437716284928460035340268566334323673088545157841522913";
	
		 BigInteger E = new BigInteger(String.valueOf(e));
		 BigInteger D = new BigInteger(String.valueOf(d));
		 BigInteger N = new BigInteger(String.valueOf(n));
		 RSA rsa = new RSA(E,D,N);
		 
		 byte[] decryptedClientId = rsa.decrypt(EncryptedTgs);   
		 String decryptTGS = new String(decryptedClientId);
		 
		 byte[] decryptedAS = rsa.decrypt(encryptedValueFromAS);
		 String decryptAS_TGS = new String(decryptedAS);
		 
	     System.out.println("Decrypted Client hardware Address: " + decryptTGS);
	     System.out.println("Decrypted Ticket Meassage from AS: " + decryptAS_TGS);
	     
	     String[] hardwareId = decryptTGS.split("-");
	     biosC = hardwareId[0]; 
	     motherboardC = hardwareId[1]; 
	     diskC = hardwareId[2]; 
	     ramC = hardwareId[3]; 
	     currentIpC = hardwareId[4];
	     currentTimestampC = hardwareId[5].toString();
	     String email = hardwareId[6];
	     
	    // System.out.println(biosC +" "+ motherboardC+" "+diskC+" "+ramC+ " "+ currentIpC+ " "+currentTimestampC);
	     //System.out.println("Email: "+ email);
	     
	     String[] hardwareIdAS = decryptAS_TGS.split("-");
	     biosAS = hardwareIdAS[0]; 
	     motherboardAS = hardwareIdAS[1]; 
	     diskAS = hardwareIdAS[2]; 
	     ramAS = hardwareIdAS[3]; 
	     currentIpAS = hardwareIdAS[4];
	     currentTimestampAS = hardwareIdAS[5].toString();
	     
	     //System.out.println("string value:"+currentTimestampAS);
	     //System.out.println("string value:"+currentTimestampC);
	     
	     
	     if(biosC.equals(biosAS) && motherboardC.equals(motherboardAS)
	    		 && diskC.equals(diskAS)&& currentIpC.equals(currentIpAS)){
	    	 //JOptionPane.showMessageDialog(null, "HardwareAddress are Match");
	    	 
	    	 long timestampAS = Long.parseLong(currentTimestampAS);
	    	 long timestampClient = Long.parseLong(currentTimestampC);
	    	 long difference = timestampClient - timestampAS;
	    	// System.out.println(timestampClient);
	    	 //System.out.println(timestampAS);
	    	 
	    	 if(difference < 4*1000){
	    		 //JOptionPane.showMessageDialog(null, "Timestamp Match");
	    		 System.out.println("Timestap Difference: "+difference+ "ms");
	    		 //System.out.println(timestampClient+"\n"+ timestampAS);
		    	 Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		    	 long currentTimestamp = timestamp.getTime();
		    	 //System.out.println("TimeStamp in Clientsite: "+currentTimestamp);
		    	 String plantext =biosAS+"-"+motherboardAS+"-"+diskAS+"-"+ramAS+"-"+currentIpAS+"-"+currentTimestamp;
		    	 
		   
		    	 query = "select * from ClientTable where Email='"+email.toString()+"'";
		 		
		    	 try {
		 			PreparedStatement pst = conn.prepareStatement(query);
		 			ResultSet rs = pst.executeQuery();
		 			while(rs.next()){
		 				keyvalue =rs.getString("Key");
		 				//System.out.println("Key getting in centeral DB: " + keyvalue);
		 			}
		 		} catch (SQLException e1) {
		 			// TODO Auto-generated catch block
		 			e1.printStackTrace();
		 		}
		    	
		    	 byte[] decodedKey = Base64.getDecoder().decode(keyvalue);
				SecretKeySpec originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES"); 
					
				//System.out.println("Key in TGS: "+originalKey);
				
				String Ke ="8457058329558680998305572832100450395974532750999602353224758683159897261009914954174902244717320365471122070084924641798535169818272360234331500509851379";
				String Kd ="12347224390756802089706431081061887804590129706575709814111512995424444690666085058122369323568248268751986384162215431665215550147431570987785738232263536180675903179458775577222707562505072239944171738036937975393319702165143665347646685164858476704620401419868526268854210941669339788158078628412519932309380579864114867811425142544131531576099489465312011864347351036971074515677039642114005038016171888989244865034221391572747347394427347735802032563445500057334805464668768481710313668036472957987580446121545866034783394212982397632064403802737118185442634362287685341137264064630268767811866069079117329501819";
				String Kn ="19777160832291450451196169894711754715127584074249328684794373670410631171150664525438672052201617138575902771170885954256428424689355811240255213401525009215881003617701502753605517116977730094483795121487172462764679162437588583481726723277023704198750280896478723954184828441018956183242375141862676824904157597499038574426059194417885036971738074822805305707547836223384896789844662443013868597567270876705403739243937349854648285018622958053805415174287838907718396750558303563861793333886854853743174927188676075082747837826097896060318293432373630035119551336545238274449573361206024620140100863683484693331423";
				
				RSA rsa1 = new RSA(E,D,N);
				
				byte[] encryptedE= rsa1.encrypt(Ke.getBytes());
				System.out.println("Encrypted File Server E in bytes: "+encryptedE);
				
				byte[] decryptedE = rsa.decrypt(encryptedE);
				System.out.println(new String(decryptedE));
				
				byte[] encryptedN=rsa1.encrypt(Kn.getBytes());
				System.out.println("Encrypted File Server N in bytes: "+encryptedN);
				
				byte[] decryptedN = rsa.decrypt(encryptedN);
				System.out.println(new String(decryptedN));
				//byte[] decryptedN = rsa.decrypt(encryptedN);
				//System.out.println(new String(decryptedN));
				
				BigInteger Ee = new BigInteger(String.valueOf(Ke));
				BigInteger De = new BigInteger(String.valueOf(Kd));
				BigInteger Nn = new BigInteger(String.valueOf(Kn));
				
				RSA2 rSA = new RSA2(Ee, De, Nn);
				 
				byte[] encrypted = rSA.encrypt(plantext.getBytes());
				 
				System.out.println("Encrypted Hardware Address bytes: "+encrypted);
				
				byte[] deme = rSA.decrypt(encrypted);
				
				System.out.println("Decrypted Hardware Address : "+ new String(deme));
				//Client client = new Client();
				
				//client.messageFromTGS(encryptedE,encryptedN,encrypted);
		    	 
	    	 }
	    	 else{
	    		 JOptionPane.showMessageDialog(null, "Timestamp not Match");
	    		 Firstpage.main(null);
	    	 }
	     }
	      
	}

}
